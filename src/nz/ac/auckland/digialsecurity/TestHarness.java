package nz.ac.auckland.digialsecurity;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringBufferInputStream;
import java.io.StringReader;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class TestHarness
 */
@WebServlet("/TestHarness")
public class TestHarness extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TestHarness() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("Start Get Request: TestHarness");
		
		Enumeration<String> headers = request.getHeaders("content-length");
		while (headers.hasMoreElements()) {
			String header = (String) headers.nextElement();
			System.out.println(header);
		}
		
		response.getWriter().append("Get TestHarness");
		System.out.println("End Get Request: TestHarness");
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		ServletInputStream in = request.getInputStream();
		
		System.out.println("Start of Request Body: TestHarness");
		
		Enumeration<String> headers = request.getHeaders("content-length");
		while (headers.hasMoreElements()) {
			String header = (String) headers.nextElement();
			System.out.println(header);
		}
		
		int nRead;
		byte[] buf = new byte[16384];
		while ((nRead = in.read(buf, 0, buf.length)) != -1) {
			
			byte[] copy = new byte[nRead];
			for (int i = 0; i < nRead; i++) {
				copy[i] = buf[i];
			}
			
			System.out.println(new String(copy));
		}
		
		System.out.println("End of Request Body: TestHarness");
		
		response.getWriter().append("Post TestHarness");

	}
	
	

}
