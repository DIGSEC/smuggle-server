package nz.ac.auckland.digialsecurity;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Victim
 */
@WebServlet("/Victim")
public class Victim extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Victim() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("Start Get Request: Victim");
		response.getWriter().append("Get Victim");
		System.out.println("End Get Request: Victim");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ServletInputStream in = request.getInputStream();
		
		System.out.println("Start of Request Body: Victim");
		int nRead;
		byte[] buf = new byte[16384];
		while ((nRead = in.read(buf, 0, buf.length)) != -1) {
			
			byte[] copy = new byte[nRead];
			for (int i = 0; i < nRead; i++) {
				copy[i] = buf[i];
			}
			
			System.out.println(new String(copy));
		}
		System.out.println("End of Request Body: Victim");
		
		response.getWriter().append("Post Victim");

	}

}
